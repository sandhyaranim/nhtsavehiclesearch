import { Component, OnInit } from '@angular/core';
import { VehicleService } from '../../services/vehicle.service';
import { VehicleDataService } from '../../services/vehicle-data.service';

@Component({
  selector: 'app-vehicle-list',
  templateUrl: './vehicle-list.component.html',
  styleUrls: ['./vehicle-list.component.css']
})
export class VehicleListComponent implements OnInit {

  vehicleData = [];
  manufacturerName = null;
  constructor(private vehicleService: VehicleService,private dataService: VehicleDataService) {
    this.vehicleService.getVehicleDetails().subscribe(
      data => {
        this.vehicleData = data['Results'];
      },
      err => console.log(err),
      () => console.log("done loading vehicles")
    );
    dataService.data.subscribe(data => {
      this.manufacturerName = data;
      
    })
  }
  ngOnInit() {}

}
