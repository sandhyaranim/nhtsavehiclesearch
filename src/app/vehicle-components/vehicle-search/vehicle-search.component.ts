import { Component, OnInit } from '@angular/core';
import {
  FormControl,
  FormGroup,
  FormBuilder
} from '@angular/forms';
import { Observable } from 'rxjs/Rx';

import { VehicleDataService } from '../../services/vehicle-data.service';

@Component({
  selector: 'app-vehicle-search',
  templateUrl: './vehicle-search.component.html',
  styleUrls: ['./vehicle-search.component.css']
})
export class VehicleSearchComponent implements OnInit {
  searchField: FormControl;
  vehicleForm: FormGroup;

  constructor(private fb: FormBuilder, private dataService: VehicleDataService) {
    this.searchField = new FormControl();
    this.vehicleForm = fb.group({ search: this.searchField });
    this.searchField.valueChanges.subscribe(val => {
      this.dataService.updatedDataSelection(val);
    })

  }

  ngOnInit() {
  }

}
