import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';


import { AppComponent } from './app.component';
import { VehicleService } from './services/vehicle.service';
import { VehicleListComponent } from './vehicle-components/vehicle-list/vehicle-list.component';
import { VehicleSearchComponent } from './vehicle-components/vehicle-search/vehicle-search.component';
import { StringFilterPipe } from './filters/string-filter.pipe';
import { ReactiveFormsModule } from '@angular/forms';
import { VehicleDataService } from './services/vehicle-data.service';


@NgModule({
  declarations: [
    AppComponent,
    VehicleListComponent,
    VehicleSearchComponent,
    StringFilterPipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [VehicleService, VehicleDataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
