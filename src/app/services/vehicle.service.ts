import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class VehicleService {
  apiUrl= "https://vpic.nhtsa.dot.gov/api/vehicles/getallmanufacturers?format=json";

  constructor(private http: HttpClient) { }
  getVehicleDetails() {
   return this.http.get(this.apiUrl);
  }
}
