import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class VehicleDataService {

  private dataSource = new BehaviorSubject<String>(null);
  data = this.dataSource.asObservable();

  constructor() { }

  updatedDataSelection(data: String){
    this.dataSource.next(data);
  }

}
