import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'stringFilter'
})
export class StringFilterPipe implements PipeTransform {
  transform(value: any, term: any): any {
    if(term) {
      return value.filter((x:any) =>x.Mfr_Name.toLowerCase().includes(term.toLowerCase())
      )
    } else {
      return value;
    }
  }

}
