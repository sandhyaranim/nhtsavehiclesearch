NHTSA Vehicle Search

This  project sdispalys list of  Vehicle types manufactured by the manufacturer.
When user serach manufacturer the list get updated with given manufacturer details.
## Get started

### Clone the repo


git clone https://sandhyaranim@bitbucket.org/sandhyaranim/nhtsavehiclesearch.git
cd nhtsavehiclesearch


### Install npm packages

Install the `npm` packages described in the `package.json` and verify that it works:

npm install
ng serve

The `ng serve` command builds (compiles TypeScript and copies assets) the application into `dist/`, watches for changes to the source files, and runs on port `4200`.

Shut it down manually with `Ctrl-C`.